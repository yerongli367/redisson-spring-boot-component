package com.mimose.component.redisson.cache.api.client;

import com.mimose.component.redisson.cache.api.Cacher;
import org.redisson.api.RMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Description Redisson缓存
 * @Author ccy
 * @Date 2020/5/13
 */
public class RedissonCacher implements Cacher {

    /**
     * RMapCache的MapKey存储Key名
     */
    private static final String RMAPCACHE_MAP_KEYS = "RMAPCACHE_MAP_KEYS";

    private Map<String, ConcurrentMap<String, Object>> cacheMap = new HashMap<>();

    private RedissonClient redissonClient;

    public RedissonCacher(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    @Override
    public boolean containKeys(String mapKey, String cacheKey) {
        ConcurrentMap<String, Object> map;
        RSet<String> rMapCacheMapKeys = redissonClient.getSet(RMAPCACHE_MAP_KEYS);
        if(CollectionUtils.isEmpty(rMapCacheMapKeys) || !rMapCacheMapKeys.contains(mapKey)){
            map = redissonClient.getMap(mapKey);
        }else{
            map = redissonClient.getMapCache(mapKey);
        }
        if(!CollectionUtils.isEmpty(map)){
            if(StringUtils.isEmpty(cacheKey) || map.containsKey(cacheKey)){
                cacheMap.put(mapKey, map);
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear(String mapKey, String cacheKey) {
        if(containKeys(mapKey, cacheKey)){
            if(StringUtils.isEmpty(cacheKey)){
                cacheMap.remove(mapKey);
            }else{
                cacheMap.get(mapKey).remove(cacheKey);
            }
            clearRedisson(mapKey, cacheKey);
        }
    }

    private void clearRedisson(String mapKey, String cacheKey){
        RSet<String> rMapCacheMapKeys = redissonClient.getSet(RMAPCACHE_MAP_KEYS);
        if(CollectionUtils.isEmpty(rMapCacheMapKeys) || !rMapCacheMapKeys.contains(mapKey)){
            RMap<String, Object> map = redissonClient.getMap(mapKey);
            if(StringUtils.isEmpty(cacheKey)){
                map.clear();
            }else{
                map.remove(cacheKey);
            }
        }else{
            RMapCache<String, Object> mapCache = redissonClient.getMapCache(mapKey);
            if(StringUtils.isEmpty(cacheKey)){
                mapCache.clear();
            }else{
                mapCache.remove(cacheKey);
            }
        }
    }

    @Override
    public <T> T get(String mapKey, String cacheKey, Class<T> clazz) {
        if(containKeys(mapKey, cacheKey)){
            return clazz.cast(cacheMap.get(mapKey).get(cacheKey));
        }
        return null;
    }

    @Override
    public <T> List<T> getVIsList(String mapKey, String cacheKey, Class<T> clazz) {
        if(containKeys(mapKey, cacheKey)){
            List<Object> value = ArrayList.class.cast(cacheMap.get(mapKey).get(cacheKey));
            return value.stream().map(clazz::cast).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public <T> Map<String, T> getAll(String mapKey, Class<T> clazz) {
        if(containKeys(mapKey, null)){
            return cacheMap.get(mapKey).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, s -> clazz.cast(s.getValue())));
        }
        return null;
    }

    @Override
    public void put(String mapKey, String cacheKey, Object value) {
        ConcurrentMap<String, Object> rMap;
        if(containKeys(mapKey, null)){
            rMap = cacheMap.get(mapKey);
        }else{
            rMap = redissonClient.getMap(mapKey);
            cacheMap.put(mapKey, rMap);
        }
        rMap.put(cacheKey, value);
    }

    @Override
    public void put(String mapKey, String cacheKey, Object value, Long ttl, TimeUnit unit) {
        RMapCache<String, Object> rMap;
        if(containKeys(mapKey, null)){
            rMap = (RMapCache<String, Object>) cacheMap.get(mapKey);
        }else{
            rMap = redissonClient.getMapCache(mapKey);
            cacheMap.put(mapKey, rMap);
            // 存储RMapCache的mapKey
            RSet<String> rMapCacheMapKeys = redissonClient.getSet(RMAPCACHE_MAP_KEYS);
            rMapCacheMapKeys.add(mapKey);
        }
        rMap.put(cacheKey, value, ttl, unit);
    }
}
