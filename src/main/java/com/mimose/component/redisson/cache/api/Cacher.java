package com.mimose.component.redisson.cache.api;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Description 缓存的接口定义类
 * @Author ccy
 * @Date 2020/5/13
 */
public interface Cacher {

    boolean containKeys(String mapKey, String cacheKey);

    void clear(String mapKey, String cacheKey);

    <T> T get(String mapKey, String cacheKey, Class<T> clazz);

    <T> List<T> getVIsList(String mapKey, String cacheKey, Class<T> clazz);

    <T> Map<String, T> getAll(String mapKey, Class<T> clazz);

    void put(String mapKey, String cacheKey, Object value);

    void put(String mapKey, String cacheKey, Object value, Long ttl, TimeUnit unit);


}
